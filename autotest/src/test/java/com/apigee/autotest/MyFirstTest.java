package com.apigee.autotest;

import lombok.extern.slf4j.Slf4j;
import io.restassured.response.Response;
import org.apache.http.HttpHeaders;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@Slf4j
public class MyFirstTest {

    @Test
    public void verifyHelloWorldTest() {

        Response response = given().log().all().
                when().get("https://35.241.15.201.nip.io/hello-world");

        log.info(response.getBody().prettyPrint());
        assertThat(response.statusCode(), equalTo(HTTP_OK));

    }

    @Test
    private void verifyAuthTest() {

        log.info("Running " + Thread.currentThread().getStackTrace()[1].getMethodName() + "()...");

        // get auth token
        Response response = given().log().all().
                header("Content-Type", "application/x-www-form-urlencoded").
                formParam("client_id", "MlgBE2cbgVGzm8xxJs4rDsJehKDZ5ASokoOzlD9Ot7OQBCXa").
                formParam("client_secret", "RfLvRRRchAcaUCv1wG9POZjpRwIrFSI7ExP1yutAWeua7bufXiP9cAnp24ysWuYG").
                when().post("https://35.241.15.201.nip.io/oauth/client_credential/accesstoken?grant_type=client_credentials");

        log.info(response.getBody().prettyPrint());
        assertThat(response.statusCode(), equalTo(HTTP_OK));

        String accessToken = "Bearer " + response.body().path("access_token");
        log.info("Access token: {}", accessToken);

        // make the test call
        response = given().log().all().
                header(HttpHeaders.AUTHORIZATION, accessToken).
                when().get("https://35.241.15.201.nip.io/hellooauth2");

        log.info(response.getBody().prettyPrint());
        assertThat(response.statusCode(), equalTo(HTTP_OK));

    }
}
